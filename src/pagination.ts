import { UserModel } from "./entities/user/userModel.js";
import { Request, Response } from "express";

export const paginateMW = async (req: Request, res: Response) => {
    const { pageNumber = 0, itemsPerPage = 10 } = req.query;
    const paginatedData = await UserModel.find()
        .skip((Number(pageNumber) - 1) * Number(itemsPerPage))
        .limit(Number(itemsPerPage));
    res.status(200).json(paginatedData);
};
