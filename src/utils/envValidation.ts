import { cleanEnv, str, num } from "envalid";

export const environment = cleanEnv(process.env, {
    NODE_ENV: str({
        choices: ["development", "test", "production", "staging"],
    }),
    HOST: str({ default: "localhost" }),
    PORT: num({ default: 3030 }),
    DB_URI: str({ default: "mongodb://localhost:27017/crud-demo" }),
    APP_SECRET: str({ default: "supercalifragilisticexpialidocious" }),
    ACCESS_TOKEN_EXPIRATION: str({ default: "60m" }),
    REFRESH_TOKEN_EXPIRATION: str({ default: "60d" }),
    SALT_ROUNDS: num({ default: 10 }),
});
