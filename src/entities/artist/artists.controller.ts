import { NextFunction, Request, Response } from "express";
import { ResponseMessage } from "../../types.js";
import ArtistService from "./artistService.js";
import { ServerException } from "../../exceptions/ServerException.js";

class ArtistController {
    async getArtistByID(req: Request, res: Response) {
        const artist = await ArtistService.getArtistById(req.params.id);
        if (!artist) res.status(404).json({ status: "No artist found." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Requested Artist Attached",
                data: { artist },
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }

    async getAllArtists(req: Request, res: Response) {
        const artists = await ArtistService.getAllArtists();
        if (!artists) res.status(404).json({ status: "No artists found." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Restored All Artists",
                data: { artists },
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }

    async createArtist(req: Request, res: Response) {
        req.body.songs = [];
        const artistCreated = await ArtistService.createArtist(req.body);
        if (!artistCreated)
            res.status(404).json({ status: "artist wasnt created." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Artist Created",
                data: artistCreated,
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }

    async updateArtist(req: Request, res: Response, next: NextFunction) {
        if ("songs" in req.body) {
            next(
                new ServerException(
                    "to add song to artist please use /songs/ as POST req"
                )
            );
        }
        const updatedArtist = await ArtistService.updateArtist(
            req.params.id,
            req.body
        );
        if (!updatedArtist)
            res.status(404).json({ status: "artist wasnt updated." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Artist Details Updated",
                data: updatedArtist,
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }

    async deleteArtist(req: Request, res: Response) {
        const artistDeleted = await ArtistService.deleteArtist(req.params.id);
        if (!artistDeleted)
            return res.status(404).json({ status: "No artist found." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Artist Deleted",
                data: artistDeleted,
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }
}

const instance = new ArtistController();

export default instance;
