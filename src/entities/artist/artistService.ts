import { ServerException } from "../../exceptions/ServerException.js";
import artistDalModel from "./artistDALMongo.js";
import { Artist } from "./artistModel.js";
import songDalModel from "../song/songDALMongo.js";
import playlistDalModel from "../playlist/playlistDALMongo.js";

class ArtistService {
    async getAllArtists() {
        const artists = await artistDalModel.getAllArtists();
        return artists;
    }

    async getArtistById(artistId: string) {
        const artist = await artistDalModel.getArtistByID(artistId);
        if (!artist) throw new ServerException("artist id doesnt exist");
        return artist;
    }

    async createArtist(artist: Artist) {
        // check stage name is not taken
        const db_artists = await artistDalModel.getAllArtists();
        const isStageNameExist = db_artists.find(
            (a) => a.stage_name === artist.stage_name
        );
        if (!isStageNameExist) {
            throw new ServerException(
                "stage name is taken, please choose another"
            );
        }
        const artistCreated = await artistDalModel.createArtist(artist);
        return artistCreated;
    }

    async updateArtist(artistId: string, artistUpdateDetails: Artist) {
        //update artist
        const updatedArtist = await artistDalModel.updateArtist(
            artistId,
            artistUpdateDetails
        );
        return updatedArtist;
    }

    async deleteArtist(artistID: string) {
        const artistData = await artistDalModel.getArtistByID(artistID);
        if (!artistData) throw new ServerException("Artist ID doesnt exist");
        //delete every song from every playlist its in
        const allPlaylists = await playlistDalModel.getAllPlaylists();
        //delete songs of artist from songs collection

        const deletedArtist = await artistDalModel.deleteArtist(artistID);
        return deletedArtist;
    }
}

const instance = new ArtistService();

export default instance;
