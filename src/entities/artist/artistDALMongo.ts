import { ArtistModel, Artist } from "./artistModel.js";
import { Song } from "../song/songModel.js";
import songDalModel from "../song/songDALMongo.js";

class ArtistDALMongo {
    //get all artists
    getAllArtists = async () => {
        return await ArtistModel.find().select(`_id 
                                                full_name
                                                stage_name
                                                songs
                                                `);
    };
    //get artist by id
    getArtistByID = async (artistID: string) => {
        return await ArtistModel.findById(artistID);
    };
    //post new artist
    createArtist = async (artistInfo: Artist) => {
        return await ArtistModel.create(artistInfo);
    };
    //update artist
    updateArtist = async (artistID: string, artistInfo: Artist) => {
        return await ArtistModel.findByIdAndUpdate(artistID, artistInfo, {
            new: true,
            upsert: false,
        });
    };
    //delete artist
    deleteArtist = async (artistID: string) => {
        const curr_artist = await ArtistModel.findById(artistID);
        if (curr_artist) {
            const songs_refs = curr_artist.songs;
            const pending = songs_refs.map(
                async (song) => await songDalModel.deleteSong(song.toString())
            );
            await Promise.all(pending);
            return await ArtistModel.findByIdAndRemove(artistID);
        }
    };

    async addSongToArtist(songObj: Song) {
        const curr_artist = await ArtistModel.findById(songObj.artist);
        if (curr_artist && songObj._id) {
            curr_artist.songs.push(songObj._id);
            await curr_artist.save();
        }
    }
}

const artistDalModel = new ArtistDALMongo();

export default artistDalModel;
