import mongoose, { ObjectId, HydratedDocument } from "mongoose";
import { Song, songSchema } from "../song/songModel.js";
const { Schema, model } = mongoose;

export interface Artist {
    _id?: string;
    full_name: string;
    stage_name: string;
    songs: string[];
}

export const artistSchema = new Schema<Artist>({
    full_name: { type: String, required: true },
    stage_name: { type: String, required: true },
    songs: [{ type: Schema.Types.ObjectId, ref: "song" }],
});

export const ArtistModel = model<Artist>("artist", artistSchema);
