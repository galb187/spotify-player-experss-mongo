/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import errorWrapper from "../../middlewares/exceptions.js";
import express from "express";
import ArtistController from "./artists.controller.js";
import { Role } from "../../types.js";
import AuthController from "../auth/auth.controller.js";

class ArtistRouter {
  private readonly _router = express.Router();

  constructor() {
      this.initRouting();
  }

  private initRouting() {
      //CRUD
      //get artist by id
      this._router.get("/:id",errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])), 
      errorWrapper(ArtistController.getArtistByID));

      //get all artists
      this._router.get("/",errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])),
       errorWrapper(ArtistController.getAllArtists));

      // create new artist
      // init artist songs to empty array
      this._router.post("/",errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])),
       errorWrapper(ArtistController.createArtist));

      // update artist - optional params
      // cant update songs here
      this._router.put("/:id", errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])),
      errorWrapper(ArtistController.updateArtist));

      // delete a artist
      // delete all of his songs
      this._router.delete("/:id",errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])),
       errorWrapper(ArtistController.deleteArtist));
  }

  get router() {
      return this._router;
  }
}

const instance = new ArtistRouter();

export default instance;

