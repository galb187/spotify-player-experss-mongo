import mongoose, { Types } from "mongoose";
const { Schema, model } = mongoose;

export interface Song {
    _id?: string;
    name: string;
    genre: string[];
    duration: number;
    artist: Types.ObjectId;
    playlists: Types.ObjectId[];
}

export const songSchema = new Schema<Song>({
    name: { type: String, required: true },
    genre: [{ type: String, required: true }],
    duration: { type: Number, required: true },
    artist: { type: Schema.Types.ObjectId, ref: "artist" },
    playlists: [{ type: Schema.Types.ObjectId, ref: "playlist" }],
});

export const SongModel = model<Song>("song", songSchema);
