/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import errorWrapper from "../../middlewares/exceptions.js";
import express from "express";
import SongController from "./songs.controller.js";
import { Role } from "../../types.js";
import AuthController from "../auth/auth.controller.js";

class SongRouter {
  private readonly _router = express.Router();

  constructor() {
      this.initRouting();
  }

  private initRouting() {
      //get song by id
      this._router.get("/:id", errorWrapper(SongController.getSongByID));

      //get all songs
      this._router.get("/", errorWrapper(SongController.getAllSongs));

      // create new song
      // verify that the artist exist
      // add song to artist song list
      this._router.post("/",errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])), errorWrapper(SongController.createSong));

      // update song details
      this._router.put("/:id",errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])), errorWrapper(SongController.updateSong));

      // delete a song
      // go over all playlists refs and remove the song schema
      // delete song ref from artist list of songs
      // remove from collection
      this._router.delete("/:id",errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])), errorWrapper(SongController.deleteSong));
  }

  get router() {
      return this._router;
  }
}

const instance = new SongRouter();

export default instance;
