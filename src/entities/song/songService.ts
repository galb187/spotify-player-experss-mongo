import songDalModel from "./songDALMongo.js";
import { ServerException } from "../../exceptions/ServerException.js";
import { Song } from "./songModel.js";
import mongoose from "mongoose";
const { Types } = mongoose;

class SongService {
    async getAllSongs() {
        const songs = await songDalModel.getAllSongs();
        return songs;
    }

    async getSongById(songId: string) {
        const song = await songDalModel.getSongByID(songId);
        if (!song) throw new ServerException("song id doesnt exist");
        return song;
    }

    async createSong(songObj: Song) {
        const songCreated = await songDalModel.createSong(songObj);
        return songCreated;
    }

    async updateSong(songId: string, songUpdateDetails: Song) {
        const updatedSong = await songDalModel.updateSong(
            songId,
            songUpdateDetails
        );
        return updatedSong;
    }

    async deleteSong(songID: string) {
        const deletedSong = await songDalModel.deleteSong(songID);
        return deletedSong;
    }

    async removePlaylistRefFromSong(song_id: string, playlist_id: string) {
        const song_updated = await songDalModel.removePlaylistRefFromSong(
            song_id,
            playlist_id
        );
        return song_updated;
    }
}

const instance = new SongService();

export default instance;
