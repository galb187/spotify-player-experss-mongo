import { ArtistModel } from "../artist/artistModel.js";
import { PlaylistModel } from "../playlist/playlistModel.js";
import { Song, SongModel } from "./songModel.js";
import artistDalModel from "../artist/artistDALMongo.js";

class SongDALMongo {
    async removePlaylistRefFromSong(song_id: string, playlist_id: string) {
        const curr_song = await this.getSongByID(song_id);
        if (curr_song) {
            curr_song.playlists = curr_song.playlists.filter((pl) => {
                return pl._id.toString() !== playlist_id;
            });
            return await curr_song.save();
        }
    }

    //get all songs
    async getAllSongs() {
        return await SongModel.find().select(`_id 
                                                name 
                                                artists 
                                                `);
    }
    //get song by id
    async getSongByID(songID: string) {
        return await SongModel.findById(songID);
    }
    //post new song
    async createSong(songObj: Song) {
        const curr_artist = await ArtistModel.findById(songObj.artist);
        if (curr_artist) {
            const song_created = await SongModel.create(songObj);
            await artistDalModel.addSongToArtist(song_created);
            return song_created;
        }
    }
    //update song
    async updateSong(songID: string, songInfo: Song) {
        return await SongModel.findByIdAndUpdate(songID, songInfo, {
            new: true,
            upsert: false,
        });
    }
    //delete song
    async deleteSong(songID: string) {
        const curr_song = await SongModel.findById(songID);
        if (curr_song) {
            //go over all playlists refs and remove the song schema
            const pending = curr_song.playlists.map(async (pl_ref) => {
                const pl = await PlaylistModel.findById(pl_ref);
                if (pl) {
                    pl.songs = pl.songs.filter(
                        (s) => s._id?.toString() !== curr_song._id.toString()
                    );
                    await pl.save();
                }
            });
            await Promise.all(pending);

            //delete song ref from artist list of songs
            const artist_of_song_ref = curr_song.artist;
            const curr_artist = await ArtistModel.findById(artist_of_song_ref);
            if (curr_artist) {
                curr_artist.songs = curr_artist.songs.filter(
                    (s) => s.toString() !== curr_song._id.toString()
                );
                await curr_artist.save();
            }

            // remove from collection
            await SongModel.findByIdAndRemove(songID);
            return "deleted";
        }
    }
}

const songDalModel = new SongDALMongo();

export default songDalModel;
