import { NextFunction, Request, Response } from "express";
import { ResponseMessage } from "../../types.js";
import SongService from "./songService.js";
import { ServerException } from "../../exceptions/ServerException.js";

class SongController {
    async getSongByID(req: Request, res: Response) {
        const song = await SongService.getSongById(req.params.id);
        if (!song) res.status(404).json({ status: "No song found." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Requested Song Attached",
                data: { song },
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }

    async getAllSongs(req: Request, res: Response) {
        const songs = await SongService.getAllSongs();
        if (!songs) res.status(404).json({ status: "No songs found." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Restored All Songs",
                data: { songs },
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }

    async createSong(req: Request, res: Response, next: NextFunction) {
        const songCreated = await SongService.createSong(req.body);
        if (!songCreated)
            res.status(404).json({ status: "song wasnt created" });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Song Created",
                data: { songCreated },
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }

    async updateSong(req: Request, res: Response, next: NextFunction) {
        if ("artist" in req.body || "playlists" in req.body) {
            next(new ServerException("you can edit only details about song"));
        }
        const updatedSong = await SongService.updateSong(
            req.params.id,
            req.body
        );
        if (!updatedSong)
            res.status(404).json({ status: "song wasnt updated" });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Song Details Updated",
                data: { updatedSong },
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }

    async deleteSong(req: Request, res: Response) {
        const songDeleted = await SongService.deleteSong(req.params.id);
        if (!songDeleted)
            return res.status(404).json({ status: "No song found." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "User Deleted",
                data: { songDeleted },
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    }
}

const instance = new SongController();

export default instance;
