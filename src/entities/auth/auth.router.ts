import express from "express";
import errorWrapper from "../../middlewares/exceptions.js";
import { Role } from "../../types.js";
import authController from "./auth.controller.js";

class AuthRouter {
    private readonly _router = express.Router();

    constructor() {
        this.initRouting();
    }

    initRouting() {
        this._router.post("/register", errorWrapper(authController.registerByRole([Role.BASIC])));
        this._router.post("/login", errorWrapper(authController.login));
        this._router.get("/get_access_token", errorWrapper(authController.get_access_token));
        this._router.get("/logout",errorWrapper(authController.verifyRoleAndAuth([Role.BASIC])), errorWrapper(authController.logout));
    }

    get router() {
        return this._router;
    }
}

const instance = new AuthRouter();

export default instance;
