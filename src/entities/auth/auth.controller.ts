import { RequestHandler} from "express";
import bcrypt from "bcrypt";
import { environment } from "../../utils/envValidation.js";
import UserService from "../user/userService.js";
import jwt from "jsonwebtoken";
import ms from "ms";
import mongoose from "mongoose";
const { Types } = mongoose;

class AuthController {

    generateTokensByRoleAndID(user_id: string, roles: string[]) {
        const access_token = jwt.sign(
            { id: user_id, roles:roles, extra: "authenticationTokens" },
            environment.APP_SECRET,
            {
                expiresIn: environment.ACCESS_TOKEN_EXPIRATION, // expires in 1 minute
            }
        );
        const refresh_token = jwt.sign(
            { id: user_id, roles:roles, extra: "authenticationTokens" },
            environment.APP_SECRET,
            {
                expiresIn: environment.REFRESH_TOKEN_EXPIRATION, // expires in 60 days... long-term...
            }
        );

        return {access_token,refresh_token};
    }

    registerByRole = (roles : string[]) : RequestHandler => {
        return async (req, res, next) => {
            const { username, password } = req.body;
            //use bcrypt to encrypt user password
            const salt = await bcrypt.genSalt(environment.SALT_ROUNDS);
            const hash = await bcrypt.hash(password, salt);
            req.body.password = hash;
            req.body.refresh_token = "";
            //create user id
            const user_id = new Types.ObjectId().toString();
            // use user id to create access and refresh tokens
            const tokens = this.generateTokensByRoleAndID(user_id, roles);
            
            //set role as Basic
            req.body.roles = roles.join(";");
            //save refresh token in db
            const user_created = await UserService.createUser({
                ...req.body,
                _id: user_id,
                refresh_token: tokens.refresh_token,
            });
            //set refresh over cookie
            res.cookie("refresh_token", tokens.refresh_token, {
                maxAge: ms("60d"), //60 days
                httpOnly: true,
            });
            //return access as header
            res.setHeader("x-access-token", tokens.access_token);
            //return user without refresh token
            const user_no_token = user_created.toObject();
            delete user_no_token.refresh_token;
            res.status(200).json(user_no_token);
        };
    };

    login: RequestHandler = async (req, res, next) => {
        //use bcrypt to decrypt user password
        const { username, password } = req.body;
        const user = await UserService.getUserByUsername(username);
        if (user) {            
            const is_equal_pass = await bcrypt.compare(password, user.password);
            if (is_equal_pass) {
                // use user id to create access and refresh tokens
                const tokens = this.generateTokensByRoleAndID(user._id,user.roles.split(";"));

                const user_with_refresh_updated =
                    await UserService.updateUserRefreshToken(
                        user.id,
                        tokens.refresh_token
                    );

                //set refresh over cookie
                res.cookie("refresh_token", tokens.refresh_token, {
                    maxAge: ms("60d"), //60 days
                    httpOnly: true,
                });
                //return access as header
                res.setHeader("x-access-token", tokens.access_token);
                const user_no_token = user_with_refresh_updated?.toObject();
                delete user_no_token?.refresh_token;
                res.status(200).json(user_no_token);
            }
        }
    };

    get_access_token: RequestHandler = async (req, res, next) => {
        //get refresh_token from client - req.cookies
        const { refresh_token } = req.cookies;

        if (!refresh_token)
            return res.status(403).json({
                status: "Unauthorized",
                payload: "No refresh_token provided.",
            });

        try {
            // verifies secret and checks expiration
            const decoded = await jwt.verify(
                refresh_token,
                environment.APP_SECRET
            );

            const { id, roles} = decoded;
            //check user refresh token in DB

            //get user details from db
            const user = await UserService.getUserById(id);
            if (user.refresh_token === refresh_token) {
                const access_token = jwt.sign(
                    { id, roles, extra: "authenticationTokens" },
                    environment.APP_SECRET,
                    {
                        expiresIn: environment.ACCESS_TOKEN_EXPIRATION, //expires in 1 minute
                    }
                );
                res.setHeader("x-access-token", access_token);
                res.status(200).json({
                    status: "success",
                    new_access_token: access_token,
                });
            }
        } catch (err) {
            console.log("error: ", err);
            return res.status(401).json({
                status: "Unauthorized",
                payload: "Unauthorized - Failed to verify refresh_token.",
            });
        }
    };

    logout: RequestHandler = async (req, res, next) => {
        const remove_refresh_token_from_db = await UserService.deleteRefreshToken(req.user_id);
        res.setHeader("x-access-token", "");
        res.clearCookie("refresh_token");
        res.status(200).json({ status: "You are logged out", token_status: remove_refresh_token_from_db });
    };

    verifyRoleAndAuth = (roles : string[]) : RequestHandler => {
        return async (req, res, next) => {
            try {
                // check header or url parameters or post parameters for token
                const access_token = req.headers["x-access-token"] as string|undefined;
                
                if (!access_token)
                return res.status(403).json({
                    status: "Unauthorized",
                    payload: "No token provided.",
                });  
                // verifies secret and checks exp
                const decoded = await jwt.verify(
                    access_token,
                    environment.APP_SECRET
                    ); 
                let valid_role = false;
                roles.forEach(r => {
                    if (decoded.roles.includes(r)){
                        valid_role = true;
                    }
                });
                if(!valid_role)
                return res.status(403).json({
                    status: "Unauthorized",
                    payload: "You dont have permission to do this action",
                });                  

                // if everything is good, save to request for use in other routes
                req.user_id = decoded.id;
                next();
                
            } catch (error) {
                return res.status(401).json({
                    status: "Unauthorized",
                    payload: "Unauthorized - Failed to authenticate token. Try to request new token.",
                });
            }
        };
    };

}

const instance = new AuthController();

export default instance;



