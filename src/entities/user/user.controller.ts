import { Request, Response } from "express";
import UserService from "./userService.js";
import { ResponseMessage } from "../../types.js";

class UserController{
    async getUserByID(req: Request, res: Response){
        const user = await UserService.getUserById(req.params.id);
        if (!user) res.status(404).json({ status: "No user found." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Requested User Attached",
                data: user,
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    };
    
    async getAllUsers(req: Request, res: Response){
        const users = await UserService.getAllUsers();
        if (!users) {
            res.status(404).json({ status: "No users found." });
        } else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Restored All Users",
                data: users,
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    };
    
//    postNewUser = async (req: Request, res: Response) => {
//         req.body.role = Role.ADMIN;
//         const userCreated = await UserService.createUser(req.body);
//         const responseMsg: ResponseMessage = {
//             status: 200,
//             message: "User Created",
//             data: userCreated
//         };
//         res.status(responseMsg.status).json(responseMsg);
//     };
    
    updateUser = async (req: Request, res: Response) => {
        if("roles" in req.body){
            return res.status(404).json({ status: "can not update roles in this route" });
        }
        const userUpdated = await UserService.updateUser(req.params.id, req.body);
        if (!userUpdated) {
            return res.status(404).json({ status: "user wasnt updated." });
        } else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "User Updated",
                data: userUpdated,
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    };
    
    deleteUser = async (req: Request, res: Response) => {     
        const userDeleted = await UserService.deleteUser(req.params.id);
        if (!userDeleted) res.status(404).json({ status: "No user found." });
        else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "User Deleted",
                data: userDeleted,
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    };
}
const instance = new UserController();

export default instance;
