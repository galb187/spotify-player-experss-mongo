import { ServerException } from "../../exceptions/ServerException.js";
import { User, UserModel } from "./userModel.js";

class UserDALMongo {
    async deleteUserRefreshToken(user_id: string) {
        const user = await UserModel.findById(user_id);
        if (user) {
            user.refresh_token = "";
            await user.save();
            return "refresh token deleted";
        }else{
            return "something went wrong while deleting refresh token";
        }
    }
    async updateUserRefreshToken(id: string, refresh_token: string) {
        const user = await UserModel.findById(id);
        if (user) {
            user.refresh_token = refresh_token;
            await user.save();
            return user;
        }
    }
    async getUserByUsername(username: string) {
        return await UserModel.findOne({ username: username });
    }
    //get all users
    async getAllUsers() {
        return await UserModel.find().select(`_id 
                                                first_name 
                                                last_name 
                                                email 
                                                phone`);
    }
    //get user by id
    async getUserByID(userId:string) {
        return await UserModel.findById(userId);
    }
    //post new user
    async createUser(user: User) {
        const isExistEmail = await UserModel.findOne({
            $or: [{ username: user.username },{ email: user.email }, { phone: user.phone }],
        });
        if (isExistEmail)
            throw new ServerException("username or email or phone is already exist");
        const user_created = await UserModel.create(user);
        return user_created;
    }
    //update user
    async updateUser(user_id: string, userUpdateDetails: User) {
        const isUserExist = await UserModel.findById(user_id);
        if (!isUserExist)
            throw new ServerException("User ID incorrect, user doesnt exist");
        const user = await UserModel.findByIdAndUpdate(
            user_id,
            userUpdateDetails,
            { new: true, upsert: true }
        );
        return user;
    }

    //delete user
    async deleteUser(user_id: string) {
        const isUserExist = await UserModel.findById(user_id);
        if (!isUserExist)
            throw new ServerException("User ID incorrect, user doesnt exist");
        const user = await UserModel.findByIdAndRemove(user_id);
        return user;
    }
}

const user_mongo_dal = new UserDALMongo();

export default user_mongo_dal;
