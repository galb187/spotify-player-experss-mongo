import mongoose from "mongoose";
const { Schema, model } = mongoose;

export interface User {
    first_name: string;
    last_name: string;
    username: string;
    password: string;
    email: string;
    phone: string;
    playlists: string[];
    roles:string;
    refresh_token?: string;
}

const schema = new Schema<User>({
    first_name: { type: String, required: true },
    last_name: { type: String, required: true },
    username: { type: String, required: true },
    password: { type: String, required: true },
    email: { type: String, required: true },
    phone: { type: String, required: true },
    playlists: [{ type: Schema.Types.ObjectId, ref: "playlist" }],
    roles: { type: String },
    refresh_token: { type: String },
});

export const UserModel = model<User>("User", schema);
