import { ServerException } from "../../exceptions/ServerException.js";
import { User, UserModel } from "./userModel.js";
import user_mongo_dal from "./user.DALMongo.js";

class UserService {
    async deleteRefreshToken(user_id: string) {
        return await user_mongo_dal.deleteUserRefreshToken(user_id);
    }
    async updateUserRefreshToken(id: string, refresh_token: string) {
        return await user_mongo_dal.updateUserRefreshToken(id, refresh_token);
    }
    async getUserByUsername(username: string) {
        return await user_mongo_dal.getUserByUsername(username);
    }
    async getAllUsers() {
        const users = await user_mongo_dal.getAllUsers();
        return users;
    }

    async getUserById(userId: string) {
        const user = await user_mongo_dal.getUserByID(userId);
        if (!user) throw new ServerException("user id doesnt exist");
        return user;
    }

    async createUser(user: User) {
        const userCreated = await user_mongo_dal.createUser(user);
        return userCreated;
    }

    async updateUser(userId: string, userUpdateDetails: User) {
        const user_updated = await user_mongo_dal.updateUser(
            userId,
            userUpdateDetails
        );
        return user_updated;
    }

    async deleteUser(userId: string) {
        const user_deleted = await user_mongo_dal.deleteUser(userId);
        return user_deleted;
    }
}

const instance = new UserService();

export default instance;
