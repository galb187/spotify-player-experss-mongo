/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import errorWrapper from "../../middlewares/exceptions.js";
import express from "express";
import { validatePaginationMW } from "../../middlewares/validators/paginate.validator.js";
import {
    validate_user,
    validate_user_id,
} from "../../middlewares/validators/user.validator.js";
import UserController from "./user.controller.js";
import { paginateMW } from "../../pagination.js";
import AuthController from "../auth/auth.controller.js";
import { Role } from "../../types.js";

class UserRouter {
    private readonly _router = express.Router();

    constructor() {
        this.initRouting();
    }

    private initRouting() {
        // get paginated data
        this._router.get(
            "/paginate",
            errorWrapper(validatePaginationMW),
            errorWrapper(paginateMW)
        );

        // GETS A SINGLE USER
        this._router.get(
            "/:id",
            errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])),
            errorWrapper(UserController.getUserByID)
        );

        // GET ALL USERS
        this._router.get("/", errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])) 
        ,errorWrapper(UserController.getAllUsers));

        // Creates an admin user
        //errorWrapper(validate_user)
        this._router.post("/createAdminUser", errorWrapper(AuthController.registerByRole([Role.ADMIN])));

        // update user details 
        this._router.put(
            "/:id",
            errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])),
            errorWrapper(UserController.updateUser)
        );

        // add role to user by admin
   
        //update status (by moderator/admin) of request made by user(basic)

        //delete role by admin

        // delete user by admin
        this._router.delete(
            "/:id",
            errorWrapper(AuthController.verifyRoleAndAuth([Role.ADMIN])),
            errorWrapper(UserController.deleteUser)
        );
    }

    get router() {
        return this._router;
    }
}

const instance = new UserRouter();

export default instance;
