/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import errorWrapper from "../../middlewares/exceptions.js";
import express from "express";
import PlaylistController from "./playlists.constroller.js";
import AuthController from "../auth/auth.controller.js";
import { Role } from "../../types.js";


class PlaylistRouter {
  private readonly _router = express.Router();

  constructor() {
      this.initRouting();
  }

  private initRouting() {
      //get playlist by id
      this._router.get("/:id",errorWrapper(PlaylistController.getPlaylistByID));

      //get all playlist
      this._router.get("/", errorWrapper(PlaylistController.getAllPlaylists));

      // create new playlist - init playlist with name and empty song list
      this._router.post("/", errorWrapper(AuthController.verifyRoleAndAuth([Role.BASIC])) ,
       errorWrapper(PlaylistController.createPlaylist));

      // update playlist - update anything except song list -> dry details
      this._router.put("/:id", errorWrapper(AuthController.verifyRoleAndAuth([Role.BASIC])) ,
       errorWrapper(PlaylistController.updatePlaylistDetails));

      //update playlist - add song to playlist
      this._router.put("/:playlist_id/addSong/:song_id", errorWrapper(AuthController.verifyRoleAndAuth([Role.BASIC])),
       errorWrapper(PlaylistController.addSongToPlaylist));

      //update playlist song list -
      //delete song in play list - delete playlist ref in song and delete songSchema from playlist
      this._router.delete(
          "/:playlist_id/removeSong/:song_id", errorWrapper(AuthController.verifyRoleAndAuth([Role.BASIC])),
          errorWrapper(PlaylistController.removeSongFromPlaylist)
      );

      // delete a playlist
      // go over all songs in playlist and remove playlist ref
      // remove playlist from playlist collection
      this._router.delete("/:playlist_id",errorWrapper(AuthController.verifyRoleAndAuth([Role.BASIC])) , errorWrapper(PlaylistController.deletePlaylist));

  }

  get router() {
      return this._router;
  }
}

const instance = new PlaylistRouter();

export default instance;
