import mongoose from "mongoose";
import { Song, songSchema } from "../song/songModel.js";
import { User } from "../user/userModel.js";
const { Schema, model } = mongoose;
    
export interface Playlist {
    _id?: string;
    name: string;
    songs: Song[];
    owner: string;
}

export const playlistSchema = new Schema<Playlist>({
    name: { type: String, required: true },
    songs: [songSchema],
    owner: [{ type: Schema.Types.ObjectId, ref: "user", required: true }],
});

export const PlaylistModel = model<Playlist>("playlist", playlistSchema);
