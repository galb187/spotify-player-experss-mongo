import mongoose from "mongoose";
const { Types } = mongoose;
import { PlaylistModel, Playlist } from "./playlistModel.js";
import songDalModel from "../song/songDALMongo.js";
import { UserModel } from "../user/userModel.js";

class PlaylistDALMongo {
    //get all playlists
    async getAllPlaylists() {
        return await PlaylistModel.find().select(`_id 
                                                name 
                                                songs 
                                                `);
    }
    //get playlist by id
    async getPlaylistByID(playlistID: string) {
        return await PlaylistModel.findById(playlistID);
    }
    //post new playlist
    async createPlaylist(playlistInfo: Playlist, user_id:string) {
        const user = await UserModel.findById(user_id);
        if(user && playlistInfo._id){
            user.playlists.push(playlistInfo._id);
            await user.save();
            return await PlaylistModel.create(playlistInfo);
        }
    }
    //update playlist
    async updatePlaylist(playlistID: string, playlistInfo: Playlist) {
        return await PlaylistModel.findByIdAndUpdate(playlistID, playlistInfo, {
            new: true,
            upsert: false,
        });
    }
    //delete playlist
    async deletePlaylist(playlistID: string) {
        const curr_playlist = await PlaylistModel.findById(playlistID);
        console.log({ curr_playlist });

        if (curr_playlist) {
            const pending = curr_playlist.songs.map(async (s) => {
                if (s._id) {
                    await songDalModel.removePlaylistRefFromSong(
                        s._id,
                        playlistID
                    );
                }
            });
            await Promise.all(pending);
            return await PlaylistModel.findByIdAndRemove(playlistID);
        }
    }

    async removeSongSchemaFromPlaylist(song_id: string, playlist_id: string) {
        const curr_playlist = await this.getPlaylistByID(playlist_id);
        if (curr_playlist) {
            curr_playlist.songs = curr_playlist.songs.filter((s) => {
                return s._id?.toString() !== song_id;
            });
            return await curr_playlist.save();
        }
    }

    async addSongToPlaylist(song_id: string, playlist_id: string) {
        const curr_playlist = await this.getPlaylistByID(playlist_id);
        const song_schema_to_add = await songDalModel.getSongByID(song_id);
        if (curr_playlist && song_schema_to_add) {
            //add playlist ref to song playlists
            song_schema_to_add.playlists.push(new Types.ObjectId(playlist_id));
            await song_schema_to_add.save();
            // add song schema to playlist
            curr_playlist.songs.push(song_schema_to_add);
            await curr_playlist.save();

            return { curr_playlist, song_schema_to_add };
        }
    }
}

const playlistDalModel = new PlaylistDALMongo();

export default playlistDalModel;
