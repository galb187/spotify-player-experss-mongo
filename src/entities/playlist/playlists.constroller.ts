import { NextFunction, Request, Response } from "express";
import { ResponseMessage } from "../../types.js";
import PlaylistService from "./playlistService.js";
import { ServerException } from "../../exceptions/ServerException.js";
import SongService from "../song/songService.js";

class PlaylistController{
    async getPlaylistByID(req: Request, res: Response){
        const playlist = await PlaylistService.getPlaylistById(req.params.id);
        if (!playlist) res.status(404).json({ status: "No playlist found." });
        const responseMsg: ResponseMessage = {
            status: 200,
            message: "Requested Playlist Attached",
            data: playlist,
        };
        res.status(responseMsg.status).json(responseMsg);
    };
    
    async getAllPlaylists(req: Request, res: Response){
        const playlists = await PlaylistService.getAllPlaylists();
        if (playlists.length === 0) {
            res.status(404).json({ status: "No playlists found." });
        }
        const responseMsg: ResponseMessage = {
            status: 200,
            message: "All Playlists Attached",
            data: playlists,
        };
        res.status(responseMsg.status).json(responseMsg);
    };
    
    async createPlaylist(req: Request, res: Response){
        req.body.songs = [];
        const playlistCreated = await PlaylistService.createPlaylist(req.body, req.user_id);
        if (!playlistCreated) {
            res.status(404).json({
                status: "something went wrong, playlist was not created",
            });
        }
        const responseMsg: ResponseMessage = {
            status: 200,
            message: "Playlist Created",
            data: playlistCreated,
        };
        res.status(responseMsg.status).json(responseMsg);
    };
    
    async updatePlaylistDetails(
        req: Request,
        res: Response,
        next: NextFunction
    ){
        if ("songs" in req.body) {
            next(new ServerException("please edit song list with relevant route"));
        }
        //check insertion of other fields
        const updatedPlaylist = await PlaylistService.updatePlaylist(
            req.params.id,
            req.body
        );
        if (!updatedPlaylist) {
            res.status(404).json({
                status: "something went wrong, could not update playlist",
            });
        } else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Playlist Details Updated",
                data: updatedPlaylist,
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    };
    
    async addSongToPlaylist(req: Request, res: Response){
        const { song_id, playlist_id } = req.params;
        const updated_objects = await PlaylistService.addSongSchemaToPlaylist(
            song_id,
            playlist_id
        );
        if (!updated_objects) {
            res.status(404).json({
                status: "something went wrong, could not add song to playlist",
            });
        } else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Song was added to Playlist",
                data: { ...updated_objects },
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    };
    
    async removeSongFromPlaylist(req: Request, res: Response){
        const { song_id, playlist_id } = req.params;
        const song_with_updated_playlist_refs =
            await SongService.removePlaylistRefFromSong(song_id, playlist_id);
        const playlist_after_song_deleted =
            await PlaylistService.removeSongSchemaFromPlaylist(
                song_id,
                playlist_id
            );
        if (!song_with_updated_playlist_refs || !playlist_after_song_deleted) {
            res.status(404).json({
                status: "something went wrong, could not remove song from playlist",
            });
        } else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Song was removed from Playlist",
                data: {
                    song_with_updated_playlist_refs,
                    playlist_after_song_deleted,
                },
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    };
    
    async deletePlaylist(req: Request, res: Response){
        const { playlist_id } = req.params;
        const playlistDeleted = await PlaylistService.deletePlaylist(playlist_id);
        if (!playlistDeleted) {
            res.status(404).json({
                status: "something went wrong, could not delete playlist",
            });
        } else {
            const responseMsg: ResponseMessage = {
                status: 200,
                message: "Playlist Deleted",
                data: playlistDeleted,
            };
            res.status(responseMsg.status).json(responseMsg);
        }
    };
}

const instance = new PlaylistController();

export default instance;