import { ServerException } from "../../exceptions/ServerException.js";
import playlistDalModel from "./playlistDALMongo.js";
import { Playlist } from "./playlistModel.js";

class PlaylistService {
    async getAllPlaylists() {
        const playlists = await playlistDalModel.getAllPlaylists();
        return playlists;
    }

    async getPlaylistById(playlistId: string) {
        const pl = await playlistDalModel.getPlaylistByID(playlistId);
        return pl;
    }

    async createPlaylist(playlist: Playlist, user_id:string) {
        const playlistCreated = await playlistDalModel.createPlaylist(playlist,user_id);
        return playlistCreated;
    }

    async updatePlaylist(playlistId: string, playlistUpdateDetails: Playlist) {
        const updatedPlaylist = await playlistDalModel.updatePlaylist(
            playlistId,
            playlistUpdateDetails
        );
        return updatedPlaylist;
    }

    async deletePlaylist(playlistID: string) {
        const deletedPlaylist = await playlistDalModel.deletePlaylist(
            playlistID
        );
        return deletedPlaylist;
    }

    async removeSongSchemaFromPlaylist(song_id: string, playlist_id: string) {
        const updated_playlist =
            await playlistDalModel.removeSongSchemaFromPlaylist(
                song_id,
                playlist_id
            );
        return updated_playlist;
    }

    async addSongSchemaToPlaylist(song_id: string, playlist_id: string) {
        const updated_playlist = await playlistDalModel.addSongToPlaylist(
            song_id,
            playlist_id
        );
        return updated_playlist;
    }
}

const instance = new PlaylistService();

export default instance;
