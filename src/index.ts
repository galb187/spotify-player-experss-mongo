// #region
// import express from "express";
// import morgan from "morgan";
// import log from "@ajar/marker";
// import cors from "cors";
// import { connect_db } from "./DAL/mongoose.connection.js";
// import user_router from "./routes/users.router.js";
// import { environment } from "./utils/envValidation.js";
// import { addIDToRequest } from "./middlewares/requestAdjust.js";
// import {
//     catchErrorAndRespondMW,
//     urlNotFoundMW,
// } from "./middlewares/exceptions.js";

// import { logHttpRequestMW } from "./middlewares/loggers/users.logger.js";
// import { logErrorsMW } from "./middlewares/loggers/errors.logger.js";

// export const LOG_FILE_PATH = process.cwd() + "/src/users.log";
// export const ERROR_LOG_FILE_PATH = process.cwd() + "/src/error_log_file.log";
// export const DB_FILE_PATH = process.cwd() + "/src/data/users.json";

// const app = express();

// // middleware
// app.use(cors());
// app.use(morgan("dev"));

// //add id to each request
// app.use(addIDToRequest);
// app.use(logHttpRequestMW(LOG_FILE_PATH));

// // routing
// // app.use('/api/stories', story_router);
// app.use("/users", user_router);

// //Catch & Log Errors
// app.use(urlNotFoundMW);
// app.use(logErrorsMW(ERROR_LOG_FILE_PATH));
// app.use(catchErrorAndRespondMW);

// //start the express api server
// (async () => {
//     //connect to mongo db
//     await connect_db(environment.DB_URI);
//     await app.listen(environment.PORT, environment.HOST);
//     log.magenta(
//         "api is live on",
//         ` ✨ ⚡  http://${environment.HOST}:${environment.PORT} ✨ ⚡`
//     );
// })().catch(console.log);
// #endregion

import App from "./app.js";

App.startServer();
