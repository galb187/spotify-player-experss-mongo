import { HttpException } from "../../exceptions/HttpException.js";
import fs from "fs";
import { Request, NextFunction, Response } from "express";

export function logErrorsMW(path: string) {
    const streamer = fs.createWriteStream(path, { flags: "a" });
    return (
        error: HttpException,
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        streamer.write(
            //${req.request_id} ::
            `${error.statusCode} :: ${error.message} :: ${Date.now()} >> ${
                error.stack
            } \n`
        );
        next(error);
    };
}
