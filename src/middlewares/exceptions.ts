import { UrlNotFoundException } from "../exceptions/URLNotFound.js";
import { HttpException } from "../exceptions/httpException.js";
import { Request, NextFunction, Response, RequestHandler } from "express";

export async function urlNotFoundMW(
    req: Request,
    res: Response,
    next: NextFunction
) {
    next(new UrlNotFoundException(`URL: ${req.url} was not found`));
}

interface ErrorResponse {
    statusCode: number;
    message: string;
    url: string;
    stack?: string;
}

export async function catchErrorAndRespondMW(
    err: HttpException,
    req: Request,
    res: Response,
    next: NextFunction
) {
    const errorToSend: ErrorResponse = {
        statusCode: err.statusCode || 500,
        message: err.message || "Something went wrong",
        url: req.url,
        stack: err.stack,
    };

    res.status(errorToSend.statusCode).json(errorToSend);
}

export default function errorWrapper(routingFunc: RequestHandler) {
    return async function (req: Request, res: Response, next: NextFunction) {
        try {
            await routingFunc(req, res, next);
        } catch (err) {
            next(err);
        }
    };
}
