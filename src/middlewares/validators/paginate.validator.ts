import { NextFunction, Request, Response } from "express";
import Joi from "joi";

export const validatePaginationMW = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const paginationSchema = Joi.object({
        pageNumber: Joi.number().min(0),
        itemsPerPage: Joi.number().max(20),
    });
    req.query = await paginationSchema.validateAsync(req.query);
    next();
};
