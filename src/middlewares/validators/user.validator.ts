import { NextFunction, Request, Response } from "express";
import Joi from "joi";

export const POST_USER = "POST";
export const UPDATE_USER = "PUT";

export const validate_user = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const httpMethod = req.method;
        let first_name = Joi.string().alphanum().min(3).max(30);
        let last_name = Joi.string().alphanum().min(3).max(30);
        let email = Joi.string().email({
            minDomainSegments: 2,
            tlds: { allow: ["com", "net"] },
        });
        let phone = Joi.string()
            .length(10)
            .pattern(/^[0-9]+$/);

        if (httpMethod === POST_USER) {
            first_name = first_name.required();
            last_name = last_name.required();
            email = email.required();
            phone = phone.required();
        }
        const schema = Joi.object().keys({
            first_name,
            last_name,
            email,
            phone,
        });

        req.body = await schema.validateAsync(req.body);
        next();
    } catch (err) {
        next(err);
    }
};

export const validate_user_id = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        const idSchema = Joi.string().alphanum().min(24).max(24);
        await idSchema.validateAsync(req.params.id);
        next();
    } catch (err) {
        next(err);
    }
};
