import pkg from "mongoose";
const { connect } = pkg;
import log from "@ajar/marker";

export const connect_db = async (uri: string): Promise<void> => {
    await connect(uri);
    log.magenta(" ✨  Connected to Mongo DB ✨ ");
};
