export interface ResponseMessage {
    status: number;
    message: string;
    data: unknown;
}

export enum Role {
    BASIC = "BASIC",
    ADMIN = "ADMIN",
    MODERATOR = "MODERATOR"
  }
