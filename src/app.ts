import express, { Express } from "express";
import morgan from "morgan";
import log from "@ajar/marker";
import cors from "cors";
import { connect_db } from "./DAL/mongo/mongoose.connection.js";
import UserRouter from "./entities/user/user.router.js";
import SongRouter from "./entities/song/songs.router.js";
import ArtistRouter from "./entities/artist/artists.router.js";
import { environment } from "./utils/envValidation.js";
import AuthRouter from "./entities/auth/auth.router.js";
import PlaylistRouter from "./entities/playlist/playlists.router.js";
import { addIDToRequest } from "./middlewares/requestAdjust.js";
import {
    catchErrorAndRespondMW,
    urlNotFoundMW,
} from "./middlewares/exceptions.js";

import { logHttpRequestMW } from "./middlewares/loggers/http.logger.js";
import { logErrorsMW } from "./middlewares/loggers/errors.logger.js";
import cookieParser from "cookie-parser";

class App {
    private readonly _app: Express;
    LOG_FILE_PATH = process.cwd() + "/src/users.log";
    ERROR_LOG_FILE_PATH = process.cwd() + "/src/error_log_file.log";
    DB_FILE_PATH = process.cwd() + "/src/data/users.json";

    constructor() {
        this._app = express();
        this.initMiddleWares();
        this.initRouting();
        this.initErrorHandling();
        this.connectDataSources();
    }

    initMiddleWares() {
        this._app.use(cors());
        this._app.use(morgan("dev"));
        // parse json req.body on post routes
        this._app.use(express.json());
        //add id to each request
        this._app.use(addIDToRequest);
        //write req to log
        this._app.use(logHttpRequestMW(this.LOG_FILE_PATH));
        this._app.use(cookieParser());
    }

    initRouting() {
        this._app.use("/auth", AuthRouter.router);
        this._app.use("/users", UserRouter.router);
        this._app.use("/songs", SongRouter.router);
        this._app.use("/playlists", PlaylistRouter.router);
        this._app.use("/artists", ArtistRouter.router);
    }

    initErrorHandling() {
        //Catch & Log Errors
        this._app.use(urlNotFoundMW);
        this._app.use(logErrorsMW(this.ERROR_LOG_FILE_PATH));
        this._app.use(catchErrorAndRespondMW);
    }

    async connectDataSources() {
        await connect_db(environment.DB_URI);
    }

    async startServer() {
        await this._app.listen(environment.PORT, environment.HOST);
        log.magenta(
            "api is live on",
            ` ✨ ⚡  http://${environment.HOST}:${environment.PORT} ✨ ⚡`
        );
    }

    get app() {
        return this._app;
    }
}

const instance = new App();

export default instance;
